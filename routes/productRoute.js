const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");


// [START][ROUTE] - Create/Add a new product
router.post("/add", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)

	// Checks if the user logged in is ADMIN
	if (data.isAdmin) {
		productController.addProduct(data).then(resultFromController => res.send (resultFromController));
	} else {
		res.send (false)
	}
});

// [START][ROUTE] - Get all ACTIVE products
router.get("/allActive", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

router.get("/all", (req, res) => {
	productController.getAll().then(resultFromController => res.send(resultFromController));
});

// [START][ROUTE] - Get SPECIFIC product
router.get("/:productId", (req, res) => {

	console.log()
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// [START][ROUTE] - Edit a SPECIFIC product
router.put("/:productId", auth.verify, (req, res) => {

	// Decodes and Checks if the user logged in is ADMIN
	const data = auth.decode(req.headers.authorization);

	if (data.isAdmin) {
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false)
	}
	
});

// [START][ROUTE] - Archiving a product
router.patch("/:productId/archive", auth.verify, (req, res) => {

	// Checks if user is ADMIN or NOT
	const data = auth.decode(req.headers.authorization);

	if (data.isAdmin) {
		productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false)
	}
	
});


module.exports = router;
