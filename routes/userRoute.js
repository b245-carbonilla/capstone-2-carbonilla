const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send (resultFromController))
});

// Route for Authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send (resultFromController))
});

// Route for Creating Order
router.post("/checkout", auth.verify, async (req, res) => {
    const data = {
        userId: auth.decode(req.headers.authorization).id,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        productId: req.body.productId,
        quantity: req.body.quantity,
    };

    try {
        if (!data.isAdmin) {
            const resultFromController = await userController.createOrder(data);
            if (resultFromController) {
                res.json(true);
            } else {
                res.json(false);
            }
        } else {
            res.json(false);
        }
    } catch (error) {
        console.error(error);
        res.status(500).json(false);
    }
});


// [START][ROUTE] - Get all User
router.get("/", (req, res) => {
	userController.getAllUsers().then(resultFromController => res.send(resultFromController));
});

// [START][ROUTE] - Get Specific user
router.get("/:userId/userDetails", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	console.log(userData);

	userController.getUser({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

//Route for checking if the user's email already exists in our database
router.post("/checkEmail", (req, res) => {

	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});


module.exports = router;
