const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First Name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required."]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile Number is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [
	{
		products : [
			{
				productId: {
					required: [true, "Product Name is required."],
					type: String
				},
				quantity: {
					required: [true, "Quantity is required."],
					type: Number
				}
			}
		],
		purchasedOn : {
			default: new Date(),
			type: Date
		},
		totalAmount : {
			required: [true, "Total Amount is required."],
			type: Number
		},
	}
]
	
});

module.exports = mongoose.model("User", userSchema);

