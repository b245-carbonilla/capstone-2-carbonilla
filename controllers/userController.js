const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");
const Product = require("../models/Product");

// [START] - User Registration 
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) => {
		// Reg Failed
		if (error) {
			return false;
		// Reg Successful
		} else {
			return true;
		}
	})
}
// [END] - User Registration 

// [START] - User Authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if (result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			// Password Matched
			if (isPasswordCorrect) {
				return {access : auth.createAccessToken(result)}
			// Password didn't match
			} else {
				return false;
			}
		}
	})
}
// [END] - User Authentication

 // CREATE ORDER
module.exports.createOrder = async (data) => {
    try {
        const product = await Product.findById(data.productId);
        if (!product) {
            throw new Error("Product not found");
        }

        const user = await User.findById(data.userId);
        if (!user) {
            throw new Error("User not found");
        }

        product.ordersUser.push({ userId: data.userId });
        const productPrice = product.price;
        await product.save();

        user.orders.push({
            products: [
                {
                    productId: data.productId,
                    quantity: data.quantity,
                },
            ],

            totalAmount: data.quantity * productPrice,
        });
        await user.save();

        return { success: true };
    } catch (error) {
        return { success: false, message: error.message };
    }
};



// [START] - Get all Users
module.exports.getAllUsers = () => {
	return User.find({}).then(result => {
		return result;
	})
};

// [START] - Get Specific user
module.exports.getUser = (reqBody) => {

	return User.findById(reqBody.userId).then(result => {
		result.password = "";
		return result;
	});
};

//Checking if the email exists in the database
module.exports.checkEmailExists = (reqBody) => {

    return User.find({email:reqBody.email}).then( result => {

        if(result.length > 0) {
            return true

        //No duplicate email found // User is not yet registered in our database
        } else {
            return false
        }
    })
}

