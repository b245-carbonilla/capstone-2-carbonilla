const Product = require("../models/Product");

// [START] - Create/Add a new product
module.exports.addProduct = (data) => {

		// allows to input a new product on postman
		let newProduct = new Product ({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		});

		return newProduct.save().then((product, error) => {
			// Adding product failed
			if (error) {
				return false
			// Adding product successfull
			} else {
				return true
			}
		})
}

// [START] - Get all ACTIVE products
module.exports.getAllActive = () => {
	return Product.find({isActive:true}).then(result => {
		return result;
	})
}

module.exports.getAll = () => {
	return Product.find({}).then(result => {
		return result;
	})
}


// [START] - Get SPECIFIC product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

// [START] - Edit a SPECIFIC product
module.exports.updateProduct = (reqParams, reqBody) => {

	// "updatedProduct" will be a container
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})
};

// [START] - Archiving a product
module.exports.archiveProduct = (reqParams, reqBody) => {

	let updateActiveField = {
		isActive : reqBody.isActive,
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})
};

